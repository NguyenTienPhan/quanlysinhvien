var danhSachSinhVien = new DanhSachSinhVien();
GetStorage();
var validate = new Validation();
function DomID(id) {
  var element = document.getElementById(id);
  return element;
}
function ThemSinhVien() {
  // Lấy dữ liệu người dùng nhập vào
  var masv = DomID("masv").value;
  var hoten = DomID("hoten").value;
  var cmnd = DomID("cmnd").value;
  var email = DomID("email").value;
  var sdt = DomID("sdt").value;
  var valid = true;
  // Kiểm tra validation
  valid &= KiemTraDauVaoRong(masv, "masv") & KiemTraDauVaoRong(hoten, "hoten") 
           & KiemTraDauVaoRong(cmnd, "cmnd") & KiemTraDauVaoRong(email, "email");
  valid &= KiemTraEmail(email, "email");
  valid &= KiemTraSdt(sdt,'sdt');
 
  if (!valid) {
    return false;
  }
  // Thêm sinh viên
  var sinhVien = new SinhVien(masv, hoten,cmnd,email,sdt);
  danhSachSinhVien.ThemSinhVien(sinhVien);
  CapNhatDanhSachSinhVien(danhSachSinhVien);
function KiemTraDauVaoRong(value, id) {
    if (validate.KiemTraRong(value) == true) {
      DomID(id).style.borderColor = "green";
      return true;
    } else {
      DomID(id).style.borderColor = "red";
      return false;
    }
}
function KiemTraEmail(value, id) {
    if (validate.KiemTraEmail(value) == true) {
      DomID(id).style.borderColor = "green";
      return true;
    } else {
      DomID(id).style.borderColor = "red";
      return false;
    }
  }
  function KiemTraSdt(value, id) {
    if (validate.KiemTraSoDienThoai(value) == true) {
      DomID(id).style.borderColor = "green";
      return true;
    } else {
      DomID(id).style.borderColor = "red";
      return false;
    }
  }
  function KiemTraNumb(value, id) {
    if (validate.KiemTraTatCaLaSo(value) == true) {
      DomID(id).style.borderColor = "green";
      return true;
    } else {
      DomID(id).style.borderColor = "red";
      return false;
    }
  }
}
function CapNhatDanhSachSinhVien(modifiedDSSV){
  var listTableSV = DomID('tbodySinhVien');
  listTableSV.innerHTML = '';
  for(var i = 0; i< modifiedDSSV.DSSV.length; i++){
    // Lấy thông tin  sinh viên từ mảng sinh viên
    var sv = modifiedDSSV.DSSV[i];
    var trSinhVien = document.createElement('tr');
    var tdCheckBox = document.createElement('td');
    var chbMaSinhVien = document.createElement('input');
    chbMaSinhVien.setAttribute('class','ckbMaSV');
    chbMaSinhVien.setAttribute('type','checkbox');
    chbMaSinhVien.setAttribute('value',sv.MaSV);
    tdCheckBox.appendChild(chbMaSinhVien);
    var tdMaSV = taoTheTD('MaSV',sv.MaSV);
    var tdHoTen = taoTheTD('HoTen',sv.HoTen);
    var tdCMND = taoTheTD('CMND',sv.CMND);
    var tdEmail = taoTheTD('Email',sv.Email);
    var tdSoDT = taoTheTD('SoDT',sv.SoDT);
    // Append  các td vào tr
    trSinhVien.appendChild(tdCheckBox);
    trSinhVien.appendChild(tdMaSV);
    trSinhVien.appendChild(tdHoTen);
    trSinhVien.appendChild(tdCMND);
    trSinhVien.appendChild(tdEmail);
    trSinhVien.appendChild(tdSoDT);
    // Append các tr vào tbodySinhVien
    listTableSV.appendChild(trSinhVien);

  }
}
function taoTheTD(className,value){
  var td = document.createElement('td');
  td.className = className;
  td.innerHTML = value;
  return td;
}
function SetStorage(){
  // Chuyển đổi object mảng danh sách sinh viên thành chuỗi json
  var jsonDanhSachSinhVien = JSON.stringify(danhSachSinhVien.DSSV);
  // đem chuỗi json lưu vào storage và đặt tên 
  localStorage.setItem('DanhSachSV',jsonDanhSachSinhVien);

}
function GetStorage(){
  // Lấy ra chuỗi json là mảng danhSachSinhVien thông qua DanhSachSV
  var jsonDanhSachSinhVien = localStorage.getItem('DanhSachSV');
  var mangDSSV = JSON.parse(jsonDanhSachSinhVien);
  danhSachSinhVien.DSSV = mangDSSV;
  CapNhatDanhSachSinhVien(danhSachSinhVien);
}
// xóa sinh viên
function XoaSinhVien(){
  var listMaSV = document.getElementsByClassName('ckbMaSV');
  var listMaSVDuocChon = [];
  for(var i = 0; i < listMaSV.length;i++){
    if(listMaSV[i].checked){
      listMaSVDuocChon.push(listMaSV[i].value);
    }
  }
  danhSachSinhVien.XoaSinhVien(listMaSVDuocChon);
  CapNhatDanhSachSinhVien(danhSachSinhVien);
}
// Tìm kiếm sinh viên
function TimKiemSinhVien()
{
    var tukhoa = DomID('tukhoa').value;
    var listDanhSachSinhVienTimKiem = danhSachSinhVien.TimKiemSinhVien(tukhoa);
    CapNhatDanhSachSinhVien(listDanhSachSinhVienTimKiem);
}