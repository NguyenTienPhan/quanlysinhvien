function Validation() {
  this.KiemTraRong = function (value) {
    if (value.trim() === "") {
      return false;
    }
    return true;
  };
  this.KiemTraEmail = function (value) {
    var regexEmail =
      /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (regexEmail.test(value)) {
      return true;
    } else {
      return false;
    }
  };
  this.KiemTraSoDienThoai = function (value) {
    var regexPhone = /^\d+$/;
    if (regexPhone.test(value) && value.length >=10) {
      return true;
    }
    return false;
  };
  this.KiemTraTatCaLaSo = function(value){
      var regexNumb = /^[0-9]+$/;
      if(regexNumb.test(value)){
          return true;
      }
      return false;
  }
}
