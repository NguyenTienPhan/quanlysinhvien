function DanhSachSinhVien() {
  this.DSSV = [];
  this.ThemSinhVien = function (svThem) {
    this.DSSV.push(svThem);
  };
  this.XoaSinhVien = function (listSvXoa) {
    for (var i = 0; i < listSvXoa.length; i++) {
      for (var j = 0; j < this.DSSV.length; j++) {
        var sinhVien = this.DSSV[j];
        if (listSvXoa[i] == sinhVien.MaSV) {
          this.DSSV.splice(j, 1);
        }
      }
    }
  };
  this.SuaSinhVien = function (svUpdate) {

  };
  this.TimKiemSinhVien = function (tukhoa) {
    //List kết quả tìm kiếm : DanhSachSinhVien
    var lstKetQuaTimKiem = new DanhSachSinhVien();
    for(var i = 0; i < this.DSSV.length; i++) {
      var sv = this.DSSV[i];
      if(sv.HoTen.toLowerCase().trim().search(tukhoa.toLowerCase().trim()) != -1)
      {
        lstKetQuaTimKiem.ThemSinhVien(sv);
      }
    }
    return lstKetQuaTimKiem;
  };
}
